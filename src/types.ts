export type Logger = (message: string) => void
export type CacheableResult<T> = { element: T; expiresAfter: number }
export type CacheElementFactory<T> = () => Promise<CacheableResult<T>> | CacheableResult<T>

export interface CacheConfig {
  // Path for the snapshot file.
  snapshotPath?: string

  //Your custom logger
  logger?: Logger

  // Should seed from previous snapshot?
  reloadSnapshotOnStart?: boolean

  // Should be started in debug mode (gives you logging)
  debug?: boolean
}
