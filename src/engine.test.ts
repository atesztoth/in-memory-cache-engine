import * as fs from 'fs'

import { InMemoryCacheEngine } from './engine'

const microSleep = (ms: number): Promise<void> => new Promise((resolve) => setTimeout(resolve, ms))

jest.mock('fs')
jest.mock('path')

describe('Engine', () => {
  beforeEach(() => {
    // @ts-ignore
    fs.existsSync.mockReset()
    // @ts-ignore
    fs.readFileSync.mockReset()
    // @ts-ignore
    fs.writeFile.mockReset()
  })

  describe('No config', () => {
    const cache = new InMemoryCacheEngine()

    afterEach(() => {
      cache.purge()
    })

    test('purge works', async () => {
      cache.set('k1', { element: 'v1', expiresAfter: 9999 })
      cache.set('k2', { element: 'v2', expiresAfter: 9999 })
      cache.set('k3', { element: 'v3', expiresAfter: 9999 })

      cache.purge()

      expect(cache.count).toEqual(0)
    })

    it('should store and restore a sample string value for 1 sec', async () => {
      const res = cache.set('k1', { element: 'a', expiresAfter: 1 })
      expect(res).toEqual('a')

      await microSleep(1000)

      expect(cache.getWithoutTouch('k1')).toEqual(null)
    })

    it('should store a value forever if expiration is 0', async () => {
      const val = await cache.get('k2', async () => ({ element: 'aa', expiresAfter: 0 }))
      const element = cache.getWithoutTouch<string>('k2')

      expect(val).toEqual('aa')
      expect(element).toEqual('aa')
      cache.purge()
    })

    it('should expire a value on expire call', async () => {
      const val = await cache.get('k3', async () => ({ element: 'aa', expiresAfter: 0 }))
      const element = cache.getWithoutTouch<string>('k3')

      expect(val).toEqual('aa')
      expect(element).toEqual('aa')

      const prevVal = cache.expire('k3')

      expect(prevVal).toEqual('aa')

      const refetched = cache.getWithoutTouch<string>('k3')
      expect(refetched).toEqual(null)
    })

    it('should print how many elements it has stored', async () => {
      cache.set('a1', { element: 'a', expiresAfter: 0 })
      expect(cache.count).toEqual(1)
    })

    it('should auto-expire elements', async () => {
      cache.set('a', { element: 'val-a', expiresAfter: 1 })

      await microSleep(500)
      expect(cache.getWithoutTouch('a')).toEqual('val-a')

      await microSleep(500)
      expect(cache.getWithoutTouch('a')).toBeNull()
    })

    test('throws an error if expiresAt is below 0, set', () => {
      expect(() => {
        cache.set('kk', { element: 'df', expiresAfter: -1 })
      }).toThrow('ExpiresAfter should be greater than or equal to 0!')
    })

    test('throws an error if expiresAt is below 0, get with factory', async () => {
      await expect(() =>
        cache.get('kk', async () => ({ element: 'dfdsf', expiresAfter: -1 })),
      ).rejects.toThrowError('ExpiresAfter should be greater than or equal to 0!')
    })
  })

  describe('Custom config', () => {
    // @ts-ignore
    fs.existsSync.mockReturnValue(true)
    // @ts-ignore
    fs.readFileSync.mockReturnValue('[]')
    // @ts-ignore
    fs.writeFile.mockResolvedValue(void 0)

    const cache = new InMemoryCacheEngine({
      snapshotPath: 'a',
      reloadSnapshotOnStart: true,
      logger: jest.fn(),
      debug: true,
    })

    afterEach(() => {
      cache.purge()
    })

    it('should call snapshot reader logic', () => {
      new InMemoryCacheEngine({
        snapshotPath: 'a',
        reloadSnapshotOnStart: true,
        logger: jest.fn(),
        debug: true,
      })
      expect(fs.existsSync).toHaveBeenCalledTimes(1)
      expect(fs.existsSync).toHaveBeenCalledWith('a')
    })

    it('should call snapshot writer logic', () => {
      cache.dumpSnapshot('snapshot')
      expect(fs.writeFile).toHaveBeenCalledTimes(1)
    })

    it('should store and restore a sample string value for 1 sec', async () => {
      const res = cache.set('k1', { element: 'a', expiresAfter: 1 })
      expect(res).toEqual('a')

      await microSleep(1000)

      expect(cache.getWithoutTouch('k1')).toEqual(null)
    })

    it('should store a value forever if expiration is 0', async () => {
      const val = await cache.get('k2', async () => ({ element: 'aa', expiresAfter: 0 }))
      const element = cache.getWithoutTouch<string>('k2')

      expect(val).toEqual('aa')
      expect(element).toEqual('aa')
    })

    it('should expire a value on expire call', async () => {
      const val = await cache.get('k3', async () => ({ element: 'aa', expiresAfter: 0 }))
      const element = cache.getWithoutTouch<string>('k3')

      expect(val).toEqual('aa')
      expect(element).toEqual('aa')

      const prevVal = cache.expire('k3')

      expect(prevVal).toEqual('aa')

      const refetched = cache.getWithoutTouch<string>('k3')
      expect(refetched).toEqual(null)
    })
  })

  describe('debug mode', () => {
    const logger = jest.fn()
    const cache = new InMemoryCacheEngine({
      logger,
      snapshotPath: 'a',
      reloadSnapshotOnStart: true,
      debug: true,
    })

    beforeEach(() => {
      logger.mockReset()
    })

    afterEach(() => {
      cache.purge()
    })

    it('should start in debug mode if supplied via constructor', () => {
      const elementKey = 'elementKey'
      cache.set(elementKey, { element: 'korte', expiresAfter: 10 })
      expect(logger).toHaveBeenCalledTimes(1)
      expect(logger).toHaveBeenCalledWith(`Element added to cache for key: ${elementKey}`)

      cache.expire(elementKey)
      expect(logger).toHaveBeenCalledTimes(2)
      expect(logger).toHaveBeenCalledWith(`Element removed for key: ${elementKey}`)
    })
  })
})
