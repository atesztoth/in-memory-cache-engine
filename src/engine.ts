import * as fs from 'fs'
import * as path from 'path'

import { currentTimestamp, Maybe, removeVoidProps } from './utils'
import { CacheableResult, CacheConfig, CacheElementFactory, Logger } from './types'

type Key = string
type CachedValue<T = any> = {
  element: T
  expiresAt: number
}

const consoleLogger = (msg: string) => console.info(`[${InMemoryCacheEngine.name}] ${msg}`)

export class InMemoryCacheEngine {
  private readonly logger?: Logger
  private readonly snapshotPath: string
  private storedElements: Map<Key, CachedValue> = new Map()
  private timeoutHandlers: Map<Key, NodeJS.Timeout> = new Map()

  constructor(config?: CacheConfig) {
    const { snapshotPath, reloadSnapshotOnStart, logger, debug } = config ?? {}
    this.logger = !!(process.env.DEBUG ?? debug) ? logger ?? consoleLogger : void 'logger'
    this.snapshotPath = snapshotPath ?? path.resolve(__dirname, 'snapshot.json')

    if (reloadSnapshotOnStart === true) this.restoreFromSnapshot()
  }

  // MARK: Core
  get count(): number {
    return this.storedElements.size
  }

  purge(): void {
    for (const entry of this.storedElements.entries()) {
      const [key] = entry

      this.purgeTimeoutHandler(key)
      this.storedElements.delete(key)
    }

    this.logger?.('Cache purged.')
  }

  expire<T>(key: string): Promise<Maybe<T>> | Maybe<T> {
    const storedElement = this.storedElements.get(key)
    if (!storedElement) return null

    this.purgeTimeoutHandler(key)
    this.storedElements.delete(key)
    this.logger?.(`Element removed for key: ${key}`)

    return storedElement.element
  }

  set<T>(key: string, element: CacheableResult<T>): T {
    const { element: storableElement, expiresAfter } = element

    if (expiresAfter < 0) throw new Error('ExpiresAfter should be greater than or equal to 0!')

    this.expire(key)

    this.storedElements.set(
      key,
      removeVoidProps({
        element: storableElement,
        expiresAt: expiresAfter + currentTimestamp(),
      }) as CachedValue,
    )

    this.constructRemoveHandler(expiresAfter, key)

    this.logger?.(`Element added to cache for key: ${key}`)

    return storableElement
  }

  async get<T>(key: string, factory: CacheElementFactory<T>): Promise<Maybe<T>> {
    const storedValue = this.getWithoutTouch<T>(key)

    if (storedValue) return storedValue

    const { element, expiresAfter } = await factory()
    this.set(key, { element, expiresAfter })
    this.logger?.(`Element added to cache for key: ${key}`)

    return element as T
  }

  getWithoutTouch<T>(key: string): Maybe<T> {
    const value = this.storedElements.get(key)

    if (!value) return null

    const { expiresAt, element } = value

    if (currentTimestamp() > expiresAt) {
      this.expire(key)
      return null
    }

    return element
  }

  // MARK: Snapshot
  restoreFromSnapshot(snapshot?: string): void {
    const snapshotPath = snapshot ?? this.snapshotPath

    this.logger?.(`Looking for snapshot at location: ${snapshotPath}`)

    if (!fs.existsSync(snapshotPath)) {
      this.logger?.('Found no snapshot file.')
      return
    }

    this.logger?.('Snapshot found.')

    const seedData: [key: string, value: CachedValue][] = JSON.parse(
      fs.readFileSync(snapshotPath).toString(),
    )

    if (seedData.length < 1) {
      this.logger?.('Found snapshot file, but it contained no data.')
      return
    }

    seedData.forEach(([key, { expiresAt, element }]) => {
      const now = currentTimestamp()
      if (expiresAt < now) return

      this.storedElements.set(
        key,
        removeVoidProps({
          element: element,
          expiresAt: expiresAt,
          removeHandler: this.constructRemoveHandler(expiresAt - now, key),
        }) as CachedValue,
      )
    })

    this.logger?.(`Cache element count after re-seed: ${this.storedElements.size}`)
  }

  createSnapshot(): string {
    return JSON.stringify(
      [...this.storedElements.entries()].map(([key, cValue]) => [
        key,
        { element: cValue.element, expiresAt: cValue.expiresAt },
      ]),
      null,
      2,
    )
  }

  async dumpSnapshot(snapshot?: string): Promise<void> {
    return new Promise((resolve, reject) => {
      fs.writeFile(
        this.snapshotPath,
        snapshot ?? this.createSnapshot(),
        { encoding: 'utf8', flag: 'w' },
        (err) => {
          if (err) return reject(err)
          resolve()
        },
      )
    })
  }

  // MARK: Private
  private purgeTimeoutHandler(key: string): void {
    const timeoutHandler = this.timeoutHandlers.get(key)
    if (timeoutHandler) {
      clearTimeout(timeoutHandler)
      this.timeoutHandlers.delete(key)
    }
  }

  private constructRemoveHandler(expiresAfter: number, key: string): Maybe<NodeJS.Timeout> {
    if (expiresAfter <= 0) return null

    const timeoutHandler = setTimeout(() => {
      this.logger?.(`Removing element [${key}] because it expired.`)
      this.storedElements.delete(key)
      this.timeoutHandlers.delete(key)
    }, expiresAfter * 1000)

    this.timeoutHandlers.set(key, timeoutHandler)

    return timeoutHandler
  }
}
