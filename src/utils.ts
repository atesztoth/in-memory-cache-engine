export type Maybe<A> = A | null
export const currentTimestamp = () => Math.floor(Date.now() / 1000)

const voidOrNull = <T>(val: T): boolean => val === void null || val === null

export const removeVoidProps = <T extends Record<string, any>>(
  obj: Partial<T>,
  voidChecker: <T>(val: T) => boolean = voidOrNull,
): Partial<T> =>
  Object.keys(obj).reduce(
    (a, c) => ({
      ...a,
      ...(voidChecker(obj[c]) ? {} : { [c]: obj[c] }),
    }),
    {} as Partial<T>,
  )
