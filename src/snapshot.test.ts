import * as fs from 'fs'

import { currentTimestamp } from './utils'
import { InMemoryCacheEngine } from './engine'

const microSleep = (ms: number): Promise<void> => new Promise((resolve) => setTimeout(resolve, ms))

jest.mock('fs')
jest.mock('./utils', () => ({
  currentTimestamp: () => 1,
  removeVoidProps: (obj: any) =>
    Object.keys(obj).reduce(
      (a, c) => ({
        ...a,
        ...(obj[c] === null || obj[c] === void 1 ? {} : { [c]: obj[c] }),
      }),
      {},
    ),
}))

describe('snapshot', () => {
  const timePoint = currentTimestamp()

  it('should restore from snapshot correctly', () => {
    const snapshot = [
      ['b', { element: [1, 2, 3, 4], expiresAt: timePoint + 60 }],
      ['a', { element: 'element', expiresAt: timePoint + 61 }],
    ]

    // @ts-ignore
    fs.existsSync.mockReturnValue(true)
    // @ts-ignore
    fs.readFileSync.mockReturnValue(Buffer.from(JSON.stringify(snapshot)))
    // @ts-ignore
    fs.writeFile.mockResolvedValue(void 0)

    const newCache = new InMemoryCacheEngine()
    newCache.restoreFromSnapshot()

    expect(newCache.count).toEqual(2)
    // @ts-ignore
    expect(newCache.getWithoutTouch('b')).toEqual(snapshot[0][1].element)
    // @ts-ignore
    expect(newCache.getWithoutTouch('a')).toEqual(snapshot[1][1].element)

    newCache.purge()
  })

  it('should restore from snapshot correctly including timeout handlers', async () => {
    const snapshot = [
      ['b', { element: [1, 2, 3, 4], expiresAt: timePoint + 1 }],
      ['a', { element: 'element', expiresAt: timePoint + 2 }],
    ]

    // @ts-ignore
    fs.existsSync.mockReturnValue(true)
    // @ts-ignore
    fs.readFileSync.mockReturnValue(Buffer.from(JSON.stringify(snapshot)))
    // @ts-ignore
    fs.writeFile.mockResolvedValue(void 0)

    const newCache = new InMemoryCacheEngine()
    newCache.restoreFromSnapshot()

    expect(newCache.count).toEqual(2)
    // @ts-ignore
    expect(newCache.getWithoutTouch('b')).toEqual(snapshot[0][1].element)
    // @ts-ignore
    expect(newCache.getWithoutTouch('a')).toEqual(snapshot[1][1].element)

    await microSleep(2000)

    expect(newCache.getWithoutTouch('a')).toEqual(null)
    expect(newCache.getWithoutTouch('b')).toEqual(null)

    newCache.purge()
  })

  it('should create correct cache dump', async () => {
    const cache = new InMemoryCacheEngine()
    cache.set('a', { element: 1, expiresAfter: 4 })
    cache.set('a1', { element: 2, expiresAfter: 3 })

    const createdSnapshot = cache.createSnapshot()
    expect(createdSnapshot).toEqual(
      JSON.stringify(
        [
          ['a', { element: 1, expiresAt: 5 }],
          ['a1', { element: 2, expiresAt: 4 }],
        ],
        null,
        2,
      ),
    )

    cache.purge()
  })
})
