# In memory cache

This is a lightweight, easy to use in-memory cache.

## How to install?

- create a .npmrc file in your project
- add this line: `@atesztoth:registry=https://gitlab.com/api/v4/packages/npm/`

With npm:

```npm
npm install --save @atesztoth/in-memory-cache-engine
```

With yarn:

```yarn
yarn add @atesztoth/in-memory-cache-engine
```

## How to use?

An example usage in a TS project. (JS projects should just require the package).

```typescript
import { InMemoryCacheEngine } from "@atesztoth/in-memory-cache-engine";
```

Constructing the cache is very easy. One may supply a configuration object for the cache that has a shape like this:

```typescript
interface CacheConfig {
  // Path for the snapshot file.
  snapshotPath?: string

  //Your custom logger
  logger?: Logger

  // Should seed from previous snapshot?
  reloadSnapshotOnStart?: boolean
}
```

```typescript
// Creating the cache with a sample config (you are not required to pass anything):
const cache = new InMemoryCacheEngine({ reloadSnapshotOnStart: false })
```

One may set a value by using `set` method. \
Imagine this method as it was called `forceSet`, because this will overwrite any previously set values for the supplied
key. \
The function expects a _key_, and a _descriptor_ that describes properties of the value to be cached. \
A descriptor looks like this:

`{ element: <the element itself>, expiresAfter: <TTL, how many seconds it has to live> }`

Usage Example:

```typescript
// Setting a value that expires after 60 seconds:
const val = cache.set('key', { element: 'value', expiresAfter: 60 })

const neverExpiring = cache.set('stone-key', { element: 'value1', expiresAfter: 0 })
```

You can see an example of how one should use this in an actual application below. The `get` method expects a _key_ just
like `set`, but instead of a _descriptor_, it expects a _factory function_ that returns a _Promise_ of a _descriptor_.
Using this function the engine can construct the value for itself and store it.

```typescript
// Ideally one should use a cache via providing a factory method, like this:
imaginaryFetcherFunction = async () => {
  const targetValue = await cache.get('key-for-resource', async () => {
    const resource = await complexDbFetcherCalculatorMagicFunction()
    return { element: resource, expiresAfter: 3600 }
  })

  return targetValue
}
```

One does not always have to create a value if wants to get it. \
The method `getWithoutTouch` provides a way to obtain a value only if it exists in the cache.

```typescript
const value = cache.getWithoutTouch('sample-key')
if (!!value) {
  console.info('Value exists!') // if the value was explicitly null, it didn't exist in the cache
}
```

In order to remove a value, the `expire` method should be used.

```typescript
const value = cache.expire('sample-key')
// value will contain the element that existed previously
```

## Development

The project uses `yarn`.

Clone the repository, and edit `engine.ts` in `src` folder.

Use `yarn play` to test your changes via editing `play.ts`. This file is excluded from the final build.
