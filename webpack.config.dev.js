const { merge } = require('webpack-merge')
const base = require('./webpack.config')

const fs = require('fs')
const path = require('path')

const files = fs
  .readdirSync(path.resolve(__dirname, 'src'))
  .filter(
    (fileName) =>
      fileName.endsWith('.ts') && !fileName.endsWith('.test.ts') && !fileName.endsWith('.spec.ts'),
  )

module.exports = merge(base, {
  mode: 'development',
  entry: files.reduce((a, c) => ({ ...a, [c.slice(0, -3)]: './src/' + c }), {}),
  devtool: 'inline-source-map',
  module: {
    rules: [{ test: /\.ts$/, loader: 'ts-loader', options: { configFile: 'tsconfig.json' } }],
  },
})
