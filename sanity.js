const { InMemoryCacheEngine } = require('./dist/index.js')

const engine = new InMemoryCacheEngine()

console.info(engine.set('a', { element: 'a', expiresAfter: 1 }))
