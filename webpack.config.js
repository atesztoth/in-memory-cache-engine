const fs = require('fs')
const path = require('path')

const nodeModules = fs.readdirSync('node_modules').reduce(
  (acc, dir) => {
    if (dir === '.bin') return acc
    return Object.assign({}, acc, { [dir]: `commonjs ${dir}` })
  },
  { fs: 'commonjs fs', path: 'commonjs path' },
)

module.exports = {
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'lib'),
    clean: true,
    libraryTarget: 'commonjs2',
  },
  target: 'node',
  externals: nodeModules,
  watch: false,
  watchOptions: { ignored: /nodeModules/ },
  resolve: { extensions: ['.ts'] },
  node: { __dirname: false },
}
