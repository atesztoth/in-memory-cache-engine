const { merge } = require('webpack-merge')
const base = require('./webpack.config')

const TerserPlugin = require('terser-webpack-plugin')
const TypescriptDeclarationPlugin = require('typescript-declaration-webpack-plugin')

module.exports = merge(base, {
  mode: 'production',
  entry: { index: './src/engine.ts' },
  module: {
    rules: [{ test: /\.ts$/, loader: 'ts-loader', options: { configFile: 'tsconfig.prod.json' } }],
  },
  plugins: [
    new TerserPlugin({ test: /\.js(\?.*)?$/i, extractComments: false }),
    new TypescriptDeclarationPlugin(),
  ],
  optimization: {
    minimize: true,
  },
})
