module.exports = {
  displayName: 'In Memory Cache Engine',
  globals: {
    'ts-jest': {
      tsconfig: '<rootDir>/tsconfig.json',
    },
  },
  transform: {
    '^.+\\.[tj]s$': 'ts-jest',
  },
  moduleFileExtensions: ['ts', 'js', 'html'],
  reporters: [
    'default',
    [
      'jest-junit',
      {
        outputDirectory: '<rootDir>/.coverage',
      },
    ],
  ],
  coverageReporters: ['text', 'cobertura'],
  coverageDirectory: '<rootDir>/.coverage',
}
